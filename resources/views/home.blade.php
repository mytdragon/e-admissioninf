@extends('layouts.app')

@section('title')
    Accueil
@endsection

@push('css')

@endpush

@section('content')

  <h1>Accueil</h1>
  <p>Bienvenue sur le système de entretiens d'admission de la section INF ! Cette application a été développée dans le cadre du projet TPI par Loïc Herzig.</p>

  @push('js')

  @endpush
@endsection
