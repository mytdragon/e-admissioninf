@extends('layouts.auth')

@section('title')
    Connexion
@endsection

@section('content')
  <form class="form-login" method="POST" action="{{ route('auth.login') }}">
    {{ csrf_field() }}

    <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
        <label for="username" class="col control-label">Nom d'utilisateur</label>

        <div class="col">
            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

            @if ($errors->has('username'))
                <div class="alert alert-danger" role="alert">
                  {{ $errors->first('username') }}
                </div>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password" class="col-12 control-label">Mot de passe</label>

        <div class="col">
            <input id="password" type="password" class="form-control" name="password" required>

            @if ($errors->has('password'))
              <div class="alert alert-danger" role="alert">
                {{ $errors->first('password') }}
              </div>
            @endif
        </div>
    </div>

    <div class="form-group">
        <div class="col">
            <button type="submit" class="btn btn-primary">
                Connexion
            </button>
        </div>
    </div>
</form>
@endsection
