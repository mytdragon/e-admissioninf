@foreach ($interviews as $interview)
  <tr>
    <th scope="row">{{$interview->idInterview}}</th>
    <td>{{$interview->candidate->fullname()}}</td>
    <td>{{$interview->intStart->format('d.m.Y H:i')}}</td>
    <td>{{$interview->intLocation}}</td>
    <td>{{$interview->teachersAcronymsString()}}</td>
    <td class="actions">
      <a class="remove-interview" href="{{route('interviews.destroy', ['id' => $interview->idInterview])}}"><span class="glyphicon glyphicon-remove"></span></a>
      <a href="{{route('interviews.edit', ['id' => $interview->idInterview])}}"><span class="glyphicon glyphicon-edit"></span></a>
      <a class="show-interview" href="{{ route('interviews.show', ['id' => $interview->idInterview]) }}"><span class="glyphicon glyphicon-info-sign"></span></a>
    </td>
  </tr>
@endforeach
