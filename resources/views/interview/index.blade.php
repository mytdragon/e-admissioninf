@extends('layouts.app')

@section('title')
    Entretiens
@endsection

@push('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables.min.css?v='.config('app.version')) }}"/>
@endpush

@section('content')

  <h1>Entretiens</h1>

  @include('includes.session-message')

  <div class="mb-2">
    <form id="import-excel-file" action="{{route('interviews.import')}}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="input-group">
        <div class="custom-file">
          <input type="file" class="custom-file-input" name="file">
          <label class="custom-file-label" for="inputGroupFile04">Choisir un fichier .xls, .xlsx ou .xlsm</label>
        </div>
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="submit">Importer</button>
        </div>
      </div>
      @if ($errors->has('file'))
        <div class="alert alert-danger mt-1" role="alert">
          {{ $errors->first('file') }}
        </div>
      @endif
    </form>
  </div>

  <div class="form-inline" id="filters-interview">
    <!-- Candidates -->
    <select id="candidate-filter" multiple class="bootstrap-select filter-interview-input" title="Candidats..." data-live-search="true" data-actions-box="true">
      @foreach ($candidates as $candidate)
        <option>{{$candidate->fullname()}}</option>
      @endforeach
    </select>

    <!-- Teachers -->
    <select id="teacher-filter" multiple class="bootstrap-select filter-interview-input" title="Enseignants..." data-live-search="true" data-actions-box="true">
      @foreach ($teachers as $teacher)
        <option data-tokens="{{$teacher->teaAcronym}}">{{$teacher->teaAcronym}}</option>
      @endforeach
    </select>

    <input id="start-filter" type="text" class="form-control filter-interview-input" placeholder="Minimum..." onfocus="(this.type='date')" onblur="(this.type='text')">
    <input id="end-filter" type="text" class="form-control filter-interview-input" placeholder="Maximum..." onfocus="(this.type='date')" onblur="(this.type='text')">
    <input id="location-filter" type="text" class="form-control filter-interview-input" placeholder="Lieu...">
  </div>

  <table id="table-interviews" class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Candidat</th>
        <th scope="col">Date</th>
        <th scope="col">Lieu</th>
        <th scope="col">Enseignants</th>
        <th class="actions" scope="col">
          Actions
          <a href="{{route('interviews.create')}}"><span class="glyphicon glyphicon-plus"></span></a>
        </th>
      </tr>
    </thead>
    <tbody id="interviews-container">
        @include('interview.data')
    </tbody>
  </table>

  @include('interview.modal')

  @push('js')
    <script type="text/javascript" src="{{ asset('js/datatables.min.js?v='.config('app.version')) }}"></script>
    <script type="text/javascript" src="{{ asset('js/interview.js?v='.config('app.version')) }}"></script>
  @endpush
@endsection
