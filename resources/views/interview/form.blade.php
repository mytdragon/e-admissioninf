@extends('layouts.app')

@section('title')
  @if ($action == 'edit')
    {{ $interview->idInterview }} - Modification
  @elseif ($action == 'add')
    Ajouter un entretien
  @endif
@endsection

@push('css')

@endpush

@section('content')

  <h1>
    @if ($action == 'edit')
      {{ $interview->idInterview }} - Modification
    @elseif ($action == 'add')
      <h2>Ajouter un entretien</h2>
    @endif
  </h1>

  @include('includes.session-message')

  <form enctype="multipart/form-data"
  @if ($action == 'edit')
    method="POST" action="{{ route('interviews.update', ['id' => $interview->idInterview]) }}"
  @elseif ($action == 'add')
    method="POST" action="{{ route('interviews.store') }}"
  @endif
  >
    @if ($action == 'edit') @method('PUT') @endif
    {{ csrf_field() }}

    @if (Auth::user()->teaIsAdmin)
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Date de début</span>
          </div>
          <input required name="start[date]" type="date" class="form-control" value="{{isset($interview) ? $interview->intStart->format('Y-m-d') : old('start.date')}}">
          <input required name="start[hour]" type="number" min="0" max="23" placeholder="Heure.." class="form-control" value="{{isset($interview) ? $interview->intStart->format('G') : old('start.hour')}}">
          <input required name="start[minute]" type="number" min="0" max="59" placeholder="Minute.." class="form-control" value="{{isset($interview) ? $interview->intStart->format('i') : old('start.minute')}}">
        </div>

        @if ($errors->has('start.date'))
          <div class="alert alert-danger" role="alert">
            {{ $errors->first('start.date') }}
          </div>
        @endif
        @if ($errors->has('start.hour'))
          <div class="alert alert-danger" role="alert">
            {{ $errors->first('start.hour') }}
          </div>
        @endif
        @if ($errors->has('start.minute'))
          <div class="alert alert-danger" role="alert">
            {{ $errors->first('start.minute') }}
          </div>
        @endif
      </div>

      <div class="form-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Lieu</span>
          </div>
          <input name="location" type="text" class="form-control" value="{{isset($interview) ? $interview->intLocation : old('location')}}">
        </div>

        @if ($errors->has('location'))
          <div class="alert alert-danger" role="alert">
            {{ $errors->first('location') }}
          </div>
        @endif
      </div>
    @endif

    <div class="form-group">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">Remarque</span>
        </div>
        <textarea class="form-control" name="remark">{{isset($interview) ? $interview->intRemark : old('remark')}}</textarea>
      </div>

      @if ($errors->has('remark'))
        <div class="alert alert-danger" role="alert">
          {{ $errors->first('remark') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">Points</span>
        </div>
        <input name="points" type="number" min="0" class="form-control" value="{{isset($interview) ? $interview->intPoints : old('points')}}">
      </div>

      @if ($errors->has('points'))
        <div class="alert alert-danger" role="alert">
          {{ $errors->first('points') }}
        </div>
      @endif
    </div>

    <div class="form-group {{ $errors->has('decision') ? ' has-error' : '' }}">
      <div class="input-group">
        <div class="input-group-prepend">
          <label class="input-group-text">Décison d'admission</label>
        </div>
        <select name="decision" class="bootstrap-select">
          <option selected value> -- Choisissez la décision -- </option>
          @foreach ($decisions as $decision)
            @if ((isset($interview) && $interview->decision == $decision) || old('decision') == $decision->idDecision)
              <option selected value="{{$decision->idDecision}}">{{$decision->decCode}} - {{$decision->decName}}</option>
            @else
              <option value="{{$decision->idDecision}}">{{$decision->decCode}} - {{$decision->decName}}</option>
            @endif
          @endforeach
        </select>
      </div>

        @if ($errors->has('decision'))
            <div class="alert alert-danger" role="alert">
              {{ $errors->first('decision') }}
            </div>
        @endif
    </div>

    @if (Auth::user()->teaIsAdmin)
      <div class="form-group {{ $errors->has('candidate') ? ' has-error' : '' }}">
        <div class="input-group">
          <div class="input-group-prepend">
            <label class="input-group-text">Candidat</label>
          </div>
          <select name="candidate" class="bootstrap-select" data-live-search="true" required>
            <option disabled selected value> -- Choisissez un candidat -- </option>
            @foreach ($candidates as $candidate)
              @if ((isset($interview) && $interview->candidate == $candidate) || old('candidate') == $candidate->idCandidate)
                <option selected value="{{$candidate->idCandidate}}">{{$candidate->fullname()}}</option>
              @else
                <option value="{{$candidate->idCandidate}}">{{$candidate->fullname()}}</option>
              @endif
            @endforeach
          </select>
        </div>

          @if ($errors->has('candidate'))
              <div class="alert alert-danger" role="alert">
                {{ $errors->first('candidate') }}
              </div>
          @endif
      </div>

      <div class="form-group {{ $errors->has('teachers') ? ' has-error' : '' }}">
        <div class="input-group">
          <div class="input-group-prepend">
            <label class="input-group-text">Enseignants</label>
          </div>
          <select size='5' multiple name="teachers[]" class="two-column-ms">
            @foreach ($teachers as $teacher)
              @if ((isset($interview) && $interview->teachers->contains($teacher)) || (!empty(old('teachers')) && in_array($teacher->idTeacher, old('teachers'))) )
                <option selected value="{{$teacher->idTeacher}}">{{$teacher->fullname()}}</option>
              @else
                <option value="{{$teacher->idTeacher}}">{{$teacher->fullname()}}</option>
              @endif
            @endforeach
          </select>
        </div>

        @if ($errors->has('teachers'))
            <div class="alert alert-danger" role="alert">
              {{ $errors->first('teachers') }}
            </div>
        @endif
      </div>
    @endif


    <div class="form-group">
      <button type="submit" class="btn btn-primary">
        @if ($action == 'edit')
          Modifier
        @elseif ($action == 'add')
          Ajouter
        @endif
      </button>
    </div>

  </form>


  @push('js')

  @endpush
@endsection
