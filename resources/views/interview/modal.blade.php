<div class="modal" id="modal-interview">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title"><span id="modal-title"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>

      <div class="modal-body">
        <div class="clearfix"></div>
        <div class="infos_materiel">
          <div class="properties_box col-12">
            <!-- properties -->
            <div class="h4">Propriétés:</div>
            <div id="modal-properties">
              <p>Candidat: <span id="modal-candidate"></span></p>
              <p>Date: <span id="modal-date"></span></p>
              <p>Lieu: <span id="modal-location"></span></p>
              <p>Points: <span id="modal-points"></span></p>
              <p>Décision: <span id="modal-decision"></span></p>
            </div>

            <!-- Teachers -->
            <div class="h4">Enseignant(s):</div>
            <div id="modal-teachers">
              <ul>
                <!-- Insert <li> for one Brevet in js -->
              </ul>
          </div>

          <!-- Remark -->
          <div class="h4">Remarque:</div>
          <div>
            <p id="modal-remark"></p>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>

      <div class="modal-footer">
        <a id="modal-edit-button" href="" class="btn btn-secondary">Modifier</a>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>

    </div>
  </div>
</div>
