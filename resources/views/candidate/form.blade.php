@extends('layouts.app')

@section('title')
  @if ($action == 'edit')
    {{ $candidate->fullname() }} - Modification
  @elseif ($action == 'add')
    Ajouter un candidat
  @endif
@endsection

@push('css')

@endpush

@section('content')

  <h1>
    @if ($action == 'edit')
      {{ $candidate->fullname() }} - Modification
    @elseif ($action == 'add')
      <h2>Ajouter un candidat</h2>
    @endif
  </h1>

  @include('includes.session-message')

  <form enctype="multipart/form-data"
  @if ($action == 'edit')
    method="POST" action="{{ route('candidates.update', ['id' => $candidate->idCandidate]) }}"
  @elseif ($action == 'add')
    method="POST" action="{{ route('candidates.store') }}"
  @endif
  >
    @if ($action == 'edit') @method('PUT') @endif
    {{ csrf_field() }}

    <div class="form-group">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">N°</span>
        </div>
        <input required name="no" type="text" class="form-control" value="{{isset($candidate) ? $candidate->canNo : old('no')}}">
      </div>

      @if ($errors->has('no'))
        <div class="alert alert-danger" role="alert">
          {{ $errors->first('no') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">Prénom</span>
        </div>
        <input required name="firstname" type="text" class="form-control" value="{{isset($candidate) ? $candidate->canFirstname : old('firstname')}}">
      </div>

      @if ($errors->has('firstname'))
        <div class="alert alert-danger" role="alert">
          {{ $errors->first('firstname') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">Nom</span>
        </div>
        <input required name="lastname" type="text" class="form-control" value="{{isset($candidate) ? $candidate->canLastname : old('lastname')}}">
      </div>

      @if ($errors->has('lastname'))
        <div class="alert alert-danger" role="alert">
          {{ $errors->first('lastname') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <div class="input-group">
        <div class="input-group-prepend">
          <label class="input-group-text">Formation</label>
        </div>
        <select name="formation" class="bootstrap-select" required>
          <option disabled selected value> -- Choisissez une formation -- </option>
          @foreach ($formations as $formation)
            @if ((isset($candidate) && $candidate->formation == $formation) || old('formation') == $formation->idFormation)
              <option selected value="{{$formation->idFormation}}">{{$formation->forCode}} - {{$formation->forName}}</option>
            @else
              <option value="{{$formation->idFormation}}">{{$formation->forCode}} - {{$formation->forName}}</option>
            @endif
          @endforeach
        </select>
      </div>

        @if ($errors->has('formation'))
            <div class="alert alert-danger" role="alert">
              {{ $errors->first('formation') }}
            </div>
        @endif
    </div>

    <div class="form-group">
      <button type="submit" class="btn btn-primary">
        @if ($action == 'edit')
          Modifier
        @elseif ($action == 'add')
          Ajouter
        @endif
      </button>
    </div>

  </form>


  @push('js')

  @endpush
@endsection
