@foreach ($candidates as $candidate)
  <tr>
    <th scope="row">{{$candidate->idCandidate}}</th>
    <th>{{$candidate->canNo}}</th>
    <td>{{$candidate->canFirstname}}</td>
    <td>{{$candidate->canLastname}}</td>
    <td>{{$candidate->formation->forCode}} - {{$candidate->formation->forName}}</td>
    <td class="actions">
      <a class="remove-candidate" href="{{route('candidates.destroy', ['id' => $candidate->idCandidate])}}"><span class="glyphicon glyphicon-remove"></span></a>
      <a href="{{route('candidates.edit', ['id' => $candidate->idCandidate])}}"><span class="glyphicon glyphicon-edit"></span></a>
    </td>
  </tr>
@endforeach
