@extends('layouts.app')

@section('title')
    Candidats
@endsection

@push('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables.min.css?v='.config('app.version')) }}"/>
@endpush

@section('content')

  <h1>Candidats</h1>

  @include('includes.session-message')

  <div class="mb-2">
    <form id="import-excel-file" action="{{route('candidates.import')}}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="input-group">
        <div class="custom-file">
          <input type="file" class="custom-file-input" name="file">
          <label class="custom-file-label" for="inputGroupFile04">Choisir un fichier .xls, .xlsx ou .xlsm</label>
        </div>
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="submit">Importer</button>
        </div>
      </div>
      @if ($errors->has('file'))
        <div class="alert alert-danger mt-1" role="alert">
          {{ $errors->first('file') }}
        </div>
      @endif
    </form>
  </div>

  <table id="table-candidates" class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">N°</th>
        <th scope="col">Prénom</th>
        <th scope="col">Nom</th>
        <th scope="col">Formation</th>
        <th class="actions" scope="col">
          Actions
          <a href="{{route('candidates.create')}}"><span class="glyphicon glyphicon-plus"></span></a>
        </th>
      </tr>
    </thead>
    <tbody id="candidates-container">
        @include('candidate.data')
    </tbody>
  </table>

  @push('js')
    <script type="text/javascript" src="{{ asset('js/datatables.min.js?v='.config('app.version')) }}"></script>
    <script type="text/javascript" src="{{ asset('js/candidate.js?v='.config('app.version')) }}"></script>
  @endpush
@endsection
