<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('css/multi-select.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('css/glyphicons.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-select.css?v='.config('app.version')) }}" rel="stylesheet">

    <!-- include ressources -->
    @stack('links')
    @stack('css')

    <link href="{{ asset('css/app.css?v='.config('app.version')) }}" rel="stylesheet">
</head>

<body>
  <noscript>
    <div class="alert alert-danger" role="alert">
      <strong>Votre attention s'il vous plait !</strong> Pour utiliser cette application, merci d'activer JavaScript.
    </div>
  </noscript>


  <div class="d-flex" id="wrapper">
      <!-- Sidebar -->
      <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading"><a href="{{route('home')}}">ETML - {{ config('app.name') }}</a></div>
        <div class="list-group list-group-flush">
          @if (Auth::user()->teaIsAdmin)
            <a href="{{route('candidates.index')}}" class="list-group-item list-group-item-action bg-light">Candidats</a>
            <a href="{{route('interviews.index')}}" class="list-group-item list-group-item-action bg-light">Entretiens</a>
          @endif

          <a href="{{route('agenda.show')}}" class="list-group-item list-group-item-action bg-light">Agenda</a>
          <a href="{{route('auth.show')}}" class="list-group-item list-group-item-action bg-light">Mon compte</a>

          <a href="{{route('auth.logout')}}" class="list-group-item list-group-item-action bg-light"><span class="glyphicon glyphicon-log-out"></span> Déconnexion</a>
        </div>
      </div>
      <!-- /#sidebar-wrapper -->

      <!-- Page Content -->
      <div id="page-content-wrapper">

        <div class="container-fluid">
          <button class="btn btn-light" id="menu-toggle"><span class="navbar-toggler-icon"></span></button>

          <div class="mt-4">
            @yield('content')
          </div>
        </div>

      </div>
      <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

  <!-- Scripts -->
  <script src="{{ asset('js/jquery-3.3.1.js?v='.config('app.version')) }}"></script>
  <script src="{{ asset('js/jquery-ui.js?v='.config('app.version')) }}"></script>
  <script src="{{ asset('js/popper.min.js?v='.config('app.version')) }}"></script>
  <script src="{{ asset('js/bootstrap.min.js?v='.config('app.version')) }}"></script>
  <script src="{{ asset('js/bootstrap-select-1.13.3.js?v='.config('app.version')) }}"></script>
  <script src="{{ asset('js/defaults-fr_FR.js?v='.config('app.version')) }}"></script>
  <script src="{{ asset('js/jquery.multi-select.js?v='.config('app.version')) }}"></script>
  <script src="{{ asset('js/selects-custom.js?v='.config('app.version')) }}"></script>
  <script src="{{ asset('js/ajax-setup.js?v='.config('app.version')) }}"></script>
  <script src="{{ asset('js/date-picker.js?v='.config('app.version')) }}"></script>
  @stack('js')

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
</body>
</html>
