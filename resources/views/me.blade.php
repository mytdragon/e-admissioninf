@extends('layouts.app')

@section('title')
    Mon compte
@endsection

@push('css')

@endpush

@section('content')

  <h1>{{Auth::user()->fullname()}} - {{Auth::user()->teaAcronym}}</h1>

  @include('includes.session-message')
  <form enctype="multipart/form-data" method="POST" action="{{ route('auth.update') }}">
    @method('PUT')
    {{ csrf_field() }}

    <div class="form-group">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">Nom d'utilisateur</span>
        </div>
        <input name="username" type="text" class="form-control" value="{{Auth::user()->teaUsername}}">
      </div>

      @if ($errors->has('username'))
        <div class="alert alert-danger" role="alert">
          {{ $errors->first('username') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">Mot de passe</span>
        </div>
        <input name="password" type="password" class="form-control" value="">
      </div>

      @if ($errors->has('password'))
        <div class="alert alert-danger" role="alert">
          {{ $errors->first('password') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">Adresse mail</span>
        </div>
        <input name="email" type="email" class="form-control" value="{{Auth::user()->teaEmail}}">
      </div>

      @if ($errors->has('email'))
        <div class="alert alert-danger" role="alert">
          {{ $errors->first('email') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <button type="submit" class="btn btn-primary">
        Modifier
      </button>

      @if (!Auth::user()->teaGCAccessToken)
        <a href="{{route('gcalendar.show')}}" class="btn btn-secondary">Associer Google Calendar</a>
      @else
        <a href="{{route('gcalendar.revoke_token')}}" class="btn btn-secondary">Dissocier Google Calendar</a>
      @endif
    </div>

  </form>

  @push('js')

  @endpush
@endsection
