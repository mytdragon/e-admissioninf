@extends('layouts.app')

@section('title')
    Agenda
@endsection

@push('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('fullcalendar/core/main.css?v='.config('app.version')) }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('fullcalendar/daygrid/main.css?v='.config('app.version')) }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('fullcalendar/bootstrap/main.css?v='.config('app.version')) }}"/>
  <link href='https://use.fontawesome.com/releases/v5.0.6/css/all.css' rel='stylesheet'>
@endpush

@section('content')
  <div class="container">
    <div id='calendar'></div>
    @include('includes.session-message')
    @include('interview.modal')
  </div>

  @push('js')
    <!-- FullCalendar -->
    <script src="{{asset('fullcalendar/core/main.js?v='.config('app.version')) }}"></script>
    <script src="{{asset('fullcalendar/core/locales/fr-ch.js?v='.config('app.version')) }}"></script>
    <script src="{{asset('fullcalendar/daygrid/main.js?v='.config('app.version')) }}"></script>
    <script src="{{asset('fullcalendar/bootstrap/main.js?v='.config('app.version')) }}"></script>
    <script src="{{asset('js/long-press-event.js?v='.config('app.version'))}}"></script>
    <script>
      var events = {!! $interviews !!};
      var exportIcalURL = '{{route('interviews.export.ical')}}';
      var exportGCalendarURL = '{{route('interviews.export.gcalendar')}}';
    </script>
    <script src="{{asset('js/agenda.js')}}"></script>
  @endpush
@endsection
