<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Gest Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during gest of various eloquent
    | model that we need to display to the user.
    |
    */

    /* General */
    'addSucces' => ['title' => 'Succès!', 'msg' => 'Vous avez correctement ajouté `:model`.'],
    'editSucces' => ['title' => 'Succès!', 'msg' => 'Vous avez correctement modifié `:model`.'],
    'importationDone' => ['title' => 'Succès!', 'msg' => 'Vous avez correctement importé le fichier Excel.'],
    'modelWithNameAlreadyExists' => ['title' => 'Echec!', 'msg' => 'Le nom est déjà utilisé par un(e) autre :model.'],
    'removeSucces' => ['title' => 'Succès!', 'msg' => 'Vous avez correctement supprimé `:model`.'],

    /* Interview */
    'endBeforeStart' => ['title' => 'Echec!', 'msg' => 'L\'entretien doit se terminer après le début.'],
    'importInterviewDateFormatInvalid' => ['title' => 'Echec!', 'msg' => 'Le format de la date et heure du candidat N°:no est invalide.'],
    'importInterviewDateMissing' => ['title' => 'Echec!', 'msg' => 'La date d\'entretien du candidat N°:no est manquante.'],
    'importInterviewTeachersMissing' => ['title' => 'Echec!', 'msg' => 'Les enseignants de l\'entretien du candidat N°:no sont manquants.'],
    'importInterviewTimeMissing' => ['title' => 'Echec!', 'msg' => 'L\'heure d\'entretien du candidat N°:no est manquant.'],
    'importInterviewsSucces' => ['title' => 'Succès!', 'msg' => 'Les entretiens ont été importés.'],

    /* Candidates */
    'candidateNotExistWithCodeAndFullname' => ['title' => 'Echec!', 'msg' => 'le candidat N°:no nommé `:firstname :lastname` n\'existe pas.'],
    'importCandidateFirstnameMissing' => ['title' => 'Echec!', 'msg' => 'le prénom du candidat N°:no est manquant.'],
    'importCandidateLastnameMissing' => ['title' => 'Echec!', 'msg' => 'le nom du candidat N°:no est manquant.'],
    'importCandidatesSucces' => ['title' => 'Succès!', 'msg' => 'Les candidats ont été importés.'],

    /* Formation */
    'formationNotExistWithCode' => ['title' => 'Echec!', 'msg' => 'La formation `:code` n\'existe pas.'],

    /* Me */
    'selfEditSuccess' => ['title' => 'Succès!', 'msg' => 'Vous avez correctement modifié vos informations.'],
    'usernameAlreadyTaken' => ['title' => 'Echec!', 'msg' => 'Le nom d\'utilisateur est déjà pris.'],

    /* Teacher */
    'teacherNotExistWithAcronym' => ['title' => 'Echec!', 'msg' => 'L\'enseignant avec l\'acronyme `:acronym` n\'existe pas.'],

    /* Google calendar */
    'associationEchec' => ['title' => 'Echec!', 'msg' => 'Une erreur est survenue lors de l\'association de votre compte Google Calendar.'],
    'associationSuccess' => ['title' => 'Succès!', 'msg' => 'Votre compte Google Calendar a été associé.'],
    'dissociationEchec' => ['title' => 'Echec!', 'msg' => 'Une erreur est survenue lors de la dissociation de votre compte Google Calendar.'],
    'dissociationSuccess' => ['title' => 'Succès!', 'msg' => 'Votre compte Google Calendar a été dissocié.'],

    /* Importation general */
    'headerDateNotFound' => ['title' => 'Echec!', 'msg' => 'La colonne `:header` correspondant à la date est introuvable.'],
    'headerFirstnameNotFound' => ['title' => 'Echec!', 'msg' => 'La colonne `:header` correspondant au prénom est introuvable.'],
    'headerFormationNotFound' => ['title' => 'Echec!', 'msg' => 'La colonne `:header` correspondant à la formation est introuvable.'],
    'headerLastnameNotFound' => ['title' => 'Echec!', 'msg' => 'La colonne `:header` correspondant au nom est introuvable.'],
    'headerNoNotFound' => ['title' => 'Echec!', 'msg' => 'La colonne `:header` correspondant au numéro est introuvable.'],
    'headerTeachersNotFound' => ['title' => 'Echec!', 'msg' => 'La colonne `:header` correspondant aux enseignants est introuvable.'],
    'headerTimeNotFound' => ['title' => 'Echec!', 'msg' => 'La colonne `:header` correspondant à l\'heure est introuvable.'],
    'importDataInvalid' => ['title' => 'Echec!', 'msg' => 'Une ou plusieurs données sont invalides.'],
    'sheetNotFound' => ['title' => 'Echec!', 'msg' => 'La feuille avec le nom `:name` est introuvable dans votre fichier.'],
];
