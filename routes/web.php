<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth routes
Route::get('login','AuthController@showLoginForm')->name('auth.login');
Route::post('login','AuthController@login')->name('auth.login');
Route::get('logout','AuthController@logout')->name('auth.logout');

Route::middleware(['auth'])->group(function () {
  Route::get('/', function () {
      return view('home');
  })->name('home');

  Route::get('agenda', 'AgendaController@show')->name('agenda.show');
  Route::resource('interviews', 'InterviewController', ['only' => ['edit', 'update']]);
  Route::post('interviews/export/ical', 'InterviewController@exportIcal')->name('interviews.export.ical');
  Route::post('interviews/export/gcalendar', 'GCalendarController@export')->name('interviews.export.gcalendar');

  Route::group(['prefix' => 'gcalendar', 'as' => 'gcalendar.'], function () {
      Route::get('oauth2', 'GCalendarController@show')->name('show');
      Route::get('oauth2callback', 'GCalendarController@store')->name('oauth2callback');
      Route::get('refresh_token', 'GCalendarController@refreshToken')->name('refresh_token');
      Route::get('revoke_token', 'GCalendarController@revokeToken')->name('revoke_token');
  });

  Route::get('me', function () {
      return view('me');
  })->name('auth.show');
  Route::put('me', 'AuthController@update')->name('auth.update');

  // Require admin
  Route::middleware(['checkAdmin'])->group(function () {
      Route::resource('candidates', 'CandidateController')->except(['show']);
      Route::post('candidates/import', 'CandidateController@import')->name('candidates.import');

      Route::resource('interviews', 'InterviewController', ['except' => ['edit', 'update']]);
      Route::post('interviews/import', 'InterviewController@import')->name('interviews.import');
  });
});
