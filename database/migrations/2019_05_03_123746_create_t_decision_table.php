<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 03.05.2019
/// Description: Migration for t_decision table

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTDecisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_decision', function (Blueprint $table) {
            $table->bigIncrements('idDecision');
            $table->string('decCode', 2)->unique();
            $table->string('decName', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_decision');
    }
}
