<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 03.05.2019
/// Description: Migration for t_interview_teacher table

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTInterviewTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_interview_teacher', function (Blueprint $table) {
            $table->unsignedBigInteger('fkInterview');
            $table->unsignedBigInteger('fkTeacher');
            $table->primary(['fkInterview', 'fkTeacher']);
            $table->foreign('fkInterview')->references('idInterview')->on('t_interview')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('fkTeacher')->references('idTeacher')->on('t_teacher')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_interview_teacher');
    }
}
