<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 03.05.2019
/// Description: Migration for t_formation table

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTFormationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_formation', function (Blueprint $table) {
            $table->bigIncrements('idFormation');
            $table->string('forCode', 3)->unique();
            $table->string('forName', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_formation');
    }
}
