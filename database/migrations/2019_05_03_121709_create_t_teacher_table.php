<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 03.05.2019
/// Description: Migration for t_teacher table

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_teacher', function (Blueprint $table) {
            $table->bigIncrements('idTeacher');
            $table->string('teaUsername', 20)->unique();
            $table->string('teaPassword')->default('$2y$12$KnI6cSr5XpMSG.V/90XCP.ZY.68L2vcC7lFvLUo8eFJgxvy0C5J1e');//.Etml$1234
            $table->string('teaAcronym', 3)->unique();
            $table->string('teaFirstname');
            $table->string('teaLastname');
            $table->string('teaEmail')->nullable();
            $table->boolean('teaIsAdmin')->default(false);
            $table->text('teaGCAccessToken')->nullable();
            $table->timestamp('teaDeletedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_teacher');
    }
}
