<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 03.05.2019
/// Description: Migration for t_interview table

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTInterviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_interview', function (Blueprint $table) {
            $table->bigIncrements('idInterview');
            $table->dateTime('intStart');
            $table->dateTime('intEnd');
            $table->string('intLocation', 50)->nullable();
            $table->text('intRemark')->nullable();
            $table->tinyInteger('intPoints')->nullable();
            $table->timestamp('intDeletedAt')->nullable();
            $table->unsignedBigInteger('fkCandidate');
            $table->unsignedBigInteger('fkDecision')->nullable();
            $table->foreign('fkCandidate')->references('idCandidate')->on('t_candidate')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('fkDecision')->references('idDecision')->on('t_decision')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_interview');
    }
}
