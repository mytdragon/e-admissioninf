<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 03.05.2019
/// Description: Migration for t_candidate table

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTCandidateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_candidate', function (Blueprint $table) {
            $table->bigIncrements('idCandidate');
            $table->integer('canNo');
            $table->string('canFirstname');
            $table->string('canLastname');
            $table->timestamp('canDeletedAt')->nullable();
            $table->unsignedBigInteger('fkFormation');
            $table->foreign('fkFormation')->references('idFormation')->on('t_formation')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_candidate');
    }
}
