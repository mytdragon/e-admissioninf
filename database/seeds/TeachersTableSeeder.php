<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.05.2019
/// Description: Seedings for t_teacher table

use Illuminate\Database\Seeder;
use App\Models\Teacher;

class TeachersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Teacher::create([
            'teaUsername' => 'admin',
            'teaPassword' => '$2y$12$XjMNC97SBSmJRKb8WXi65uGQ.PG.pJGyEOf5MfKNnIoKFfJu2sY5m',//8z.ZGF3bX=N4F-aJ
            'teaAcronym' => '000',
            'teaFirstname' => 'Local',
            'teaLastname' => 'Admin',
            'teaIsAdmin' => true
        ]);
        Teacher::create([
            'teaUsername' => 'psy',
            'teaPassword' => '$2y$12$sKzFEuyC/BTqFjZwf0lJZusjKnioPhdtx4.e3OP/e0KQU3NJOWJwm',//HFH#5f7$PLkusWt74*M~A-+cEf_w:-yy,'"R~bh-]T6atQ,@t4R6'/-}sG%-4_"\yuJL5BU%<-C_Zk:q]AAR`E5=Yta\z5K8K6,ZBVVby]9&/gk)k.**\Axv7&6uH]=(X?2V7K5\:R>)vKmrFSBsb/hw[{).q'77)aG;Qv^<Q}R!pZP)g#H86JB$!ZU/_7rm)@zyMawj5"F^K7<7/Nn!^{x[k8*exWyUvbcH<&vP<(VH2Dcsn6g):kw9JGh)!fS}
            'teaAcronym' => 'PSY',
            'teaFirstname' => 'Externe',
            'teaLastname' => 'Psy'
        ]);
    }
}
