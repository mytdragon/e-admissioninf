<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.05.2019
/// Description: Seedings for t_formation table

use Illuminate\Database\Seeder;
use App\Models\Formation;

class FormationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Formation::create([
            'forCode' => 'CIN',
            'forName' => 'CFC'
        ]);
        Formation::create([
            'forCode' => 'MIN',
            'forName' => 'CFC & Maturité intégrée'
        ]);
        Formation::create([
            'forCode' => 'FPA',
            'forName' => ' CFC en formation accélérée'
        ]);
    }
}
