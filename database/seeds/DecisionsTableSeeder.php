<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.05.2019
/// Description: Seedings for t_decision table

use Illuminate\Database\Seeder;
use App\Models\Decision;

class DecisionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Decision::create([
            'decCode' => 'A',
            'decName' => 'Admis'
        ]);
        Decision::create([
            'decCode' => 'D',
            'decName' => 'désisté'
        ]);
        Decision::create([
            'decCode' => 'R',
            'decName' => 'Refusé'
        ]);
        Decision::create([
            'decCode' => 'VE',
            'decName' => 'Vient-Ensuite'
        ]);
    }
}
