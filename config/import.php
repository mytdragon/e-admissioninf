<?php

return [
    'candidates' => [
        'sheet_name' => 'Résultats_Candidats',
        'header_row' => '2',
        'header_no' => 'Numéro',
        'header_firstname' => 'Prénom',
        'header_lastname' => 'Nom',
        'header_formation' => 'Option 1'
    ],

    'interviews' => [
        'sheet_name' => 'Liste',
        'header_row' => '1',
        'header_no' => 'No',
        'header_firstname' => 'Prénom',
        'header_lastname' => 'Nom',
        'header_date' => 'Date',
        'header_time' => 'Heure',
        'header_teachers' => 'Recruteurs',
    ]
];
