/**
 * ETML
 * Author: Loïc Herzig
 * Date: 15.02.2019
 * Description: Interview js related (ajax calls and filter)
 */

var columns = ['id', 'candidate', 'start', 'location', 'teachers', 'actions'];

function convertFrenchDateToInternational(date){
  let day = date.substring(0,2);
  let month = date.substring(3,5);
  let year = date.substring(6,10);
  return year + '-' + month + '-' + day;
}

$(document).ready(function() {
  $('input[type="file"]').change(function(e){
    var fileName = e.target.files[0].name;
    $('.custom-file-label').html(fileName);
  });

  $('body').on('click', '.remove-interview', function(){
    var url = $(this).attr('href');
    var tr = $(this).parents('tr');

    $.ajax({
      type: 'DELETE',
      url: url,
      data: {
        _token : $('meta[name="csrf-token"]').attr('content')
      },
      success:function(data){
        interviewTable.row(tr).remove().draw();
      }
    });
    return false;
  });

  $('body').on('click', '.show-interview', function(){
    var url = $(this).attr('href');
    $.ajax({
      type: 'GET',
      url: url,
      data: {
        _token : $('meta[name="csrf-token"]').attr('content')
      },
      success:function(data){
        // Set var in modal
        $("#modal-title").text(data.id);
        $("#modal-candidate").text(data.candidate);
        $("#modal-date").text(data.start);
        $("#modal-location").text(data.location);
        $("#modal-points").text(data.points);
        $("#modal-decision").text(data.decision);
        $("#modal-remark").text(data.remark);
        $('#modal-edit-button').hide();
        $('#modal-teachers ul').empty();
        data.teachers.forEach(function(teacher) {
          $('#modal-teachers ul').append("<li>" + teacher + "</li>");
        });

        // Show modal
        $('#modal-interview').modal('show');
      }
    });
    return false;
  });

  $('body').on('change', '#candidate-filter', function(){
    var selected = [];
    $(this).children(':selected').each(function(){
      selected.push('(?=.*' + $(this).val() + ')');
    });

    interviewTable
      .columns(columns.indexOf('candidate'))
      .search(selected.join('|'), true, false, true)
      .draw();
  })

  $('body').on('change', '#teacher-filter', function(){
    var selected = [];
    $(this).children(':selected').each(function(){
      selected.push('(?=.*' + $(this).val() + ')');
    });

    interviewTable
      .columns(columns.indexOf('teachers'))
      .search(selected.join('|'), true, false, true)
      .draw();
  });

  $('body').on('keyup', '#location-filter', function(){
    interviewTable
      .columns(columns.indexOf('location'))
      .search($(this).val().trim())
      .draw();
  });

  $('body').on('change', '#start-filter, #end-filter', function(){
    interviewTable.draw();
  });

  $.fn.dataTable.ext.search.push(
    // Extend search for start and end dates
    function( settings, data, dataIndex ) {
      var filter = true;
      var startFilter = $('#start-filter').val();
      var endFilter = $('#end-filter').val();
      var startValue = data[columns.indexOf('start')].substring(0,10);

      startValue = convertFrenchDateToInternational(startValue);

      if (startFilter != '' &&  endFilter != ''){
        if (startValue < startFilter || startValue > endFilter){
          filter = false;
        }
      }
      else if (startFilter != ''){
        if (startValue < startFilter) { filter = false; }
      }
      else if (endFilter != ''){
        if (startValue > endFilter){ filter = false; }
      }

      return filter;
    }
  );

  var interviewTable = $('#table-interviews').DataTable({
    paging: false,
    searching: true,
    info: false,
    "columnDefs": [
      { "orderable": false, "targets": columns.indexOf('actions') }
    ],
    "language": {
      "lengthMenu": "Afficher _MENU_ entrées par page",
      "zeroRecords": "Aucune entrée trouvée - désolé",
      "info": "Affiachage de la page _PAGE_ sur _PAGES_",
      "infoEmpty": "Aucune entrée disponible",
      "infoFiltered": "(filtrée de _MAX_ entrées au total)",
      "search": "Recherche:"
    }
  });
});
