/**
 * ETML
 * Author: Loïc Herzig
 * Date: 07.02.2019
 * Description: Enable the two columns multi-select for selects with class "two-column-ms"
 *      and bootstrap-select for class with "bootstrap-select"
 */

$('.two-column-ms').multiSelect();

$('.bootstrap-select').selectpicker({});
