/**
 * ETML
 * Author: Loïc Herzig
 * Date: 15.02.2019
 * Description: Interview js related (ajax call and filter)
 */


$(document).ready(function() {
  $('input[type="file"]').change(function(e){
    var fileName = e.target.files[0].name;
    $('.custom-file-label').html(fileName);
  });
  
  $('body').on('click', '.remove-candidate', function(){
    var url = $(this).attr('href');
    var tr = $(this).parents('tr');

    $.ajax({
      type: 'DELETE',
      url: url,
      data: {
        _token : $('meta[name="csrf-token"]').attr('content')
      },
      success:function(data){
        candidateTable.row(tr).remove().draw();
      }
    });
    return false;
  });

  var candidateTable = $('#table-candidates').DataTable({
    paging: false,
    searching: true,
    info: false,
    "columnDefs": [
      { "orderable": false, "targets": 5 }
    ],
    "language": {
      "lengthMenu": "Afficher _MENU_ entrées par page",
      "zeroRecords": "Aucune entrée trouvée - désolé",
      "info": "Affiachage de la page _PAGE_ sur _PAGES_",
      "infoEmpty": "Aucune entrée disponible",
      "infoFiltered": "(filtrée de _MAX_ entrées au total)",
      "search": "Recherche:"
    }
  });
});
