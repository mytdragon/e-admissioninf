/**
 * ETML
 * Author: Loïc Herzig
 * Date: 15.02.2019
 * Description: Fullcalandar
 * Require var:
 *  events (json array of events to put in the calendar)
 *  exportIcalURL
 *  exportGCalendarURL
 */

var selected = [];
var allSelected = false;

var longpress = 200;
var start;

document.addEventListener('DOMContentLoaded', function() {
  $('body').on('DOMNodeInserted', '.fc-more-popover', function () {
    $(this).find('a.fc-event').each(function (index){
      if (selected.includes( String($(this).data('id')) )){
        $(this).addClass('event-selected');
      }
    });
  });

  var calendarEl = document.getElementById('calendar');
  var calendar = new FullCalendar.Calendar(calendarEl, {
    plugins: [ 'dayGrid', 'bootstrap' ],
    customButtons: {
      exportICal: {
        text: 'iCalendar',
        click: function() {
          $.ajax({
            type: 'POST',
            url: exportIcalURL,
            data: {
              _token : $('meta[name="csrf-token"]').attr('content'),
              interviews: selected
            },
            success:function(data){
              var hiddenElement = document.createElement('a');
              hiddenElement.href = 'data:attachment/text,' + encodeURI(data);
              hiddenElement.target = '_blank';
              hiddenElement.download = 'interviews.ics';
              hiddenElement.click();
            }
          });
        }
      },
      exportICalMail: {
        text: 'iCalendar',
        click: function() {
          $.ajax({
            type: 'POST',
            url: exportIcalURL,
            data: {
              _token : $('meta[name="csrf-token"]').attr('content'),
              interviews: selected,
              mail: true
            },
            success:function(data){
              if (data.error){
                alert(data.error);
              }
              else {
                alert('Un email avec le fichier iCal a été envoyé!');
              }
            }
          });
        }
      },
      exportGCalendar: {
        text: 'Google Calendar',
        click: function() {
          $.ajax({
            type: 'POST',
            url: exportGCalendarURL,
            data: {
              _token : $('meta[name="csrf-token"]').attr('content'),
              interviews: selected
            },
            success:function(data){
              if (data.error){
                alert(data.error);
              }
              else {
                alert('Exportation effectuée avec succès!');
              }
            }
          });
        }
      },
      selectAll: {
        text: 'Tout sélectionner',
        click: function() {
          var selectButton = $('.fc-selectAll-button');
          selected = [];
          if (!allSelected) {

            $('.fc-event').each(function (index) {
              selected.push( String($(this).addClass('event-selected').data('id')) );
            });

            selectButton.text('Tout désélectionner');
          }
          else {
            $('.fc-event').each(function (index) {
              $(this).removeClass('event-selected')
            });

            selectButton.text('Tout sélectionner');
          }

          allSelected = !allSelected;
          $('.fc-exportICal-button, .fc-exportICalMail-button, .fc-exportGCalendar-button').attr('disabled', !(selected.length > 0));
        }
      }
    },
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridDay,dayGridWeek,dayGridMonth'
    },
    footer: {
      left: 'selectAll',
      right: 'exportICal,exportICalMail,exportGCalendar'
    },
    defaultView: 'dayGridMonth',
    locale: 'fr-ch',
    themeSystem: 'bootstrap',
    eventLimit: true,
    events: events,
    eventRender: function(info) {
      $(info.el).attr('data-id', info.event.id);

      if (selected.includes( String(info.event.id) )){
        $(info.el).addClass('event-selected');
      }

      info.el.addEventListener('long-press', function(e) {
        // stop the event from bubbling up
        e.preventDefault()

        eventClick(info, true);
      });
    },
    eventClick: function(info) {
      eventClick(info);
    },
  });
  calendar.render();

  $('.fc-exportICal-button, .fc-exportICalMail-button, .fc-exportGCalendar-button').removeClass('btn-primary').addClass('btn-secondary').attr('disabled', !(selected.length > 0));
  $('.fc-exportICal-button').append(' <span class="glyphicon glyphicon-download-alt"></span>');
  $('.fc-exportICalMail-button').append(' <span class="glyphicon glyphicon-envelope"></span>');
  $('.fc-selectAll-button').attr('value', 0);

  function eventClick(info, long = false) {
    if (long || info.jsEvent.ctrlKey) {
      // Select if ctrl is pressed
      if (selected.includes(info.event.id)){
        selected.splice(selected.indexOf(info.event.id),1);
        $("a[data-id='" + info.event.id + "']").removeClass('event-selected');
      }
      else {
        selected.push(info.event.id);
        $("a[data-id='" + info.event.id + "']").addClass('event-selected');
      }

      $('.fc-exportICal-button, .fc-exportICalMail-button, .fc-exportGCalendar-button').attr('disabled', !(selected.length > 0));
    }
    else {
      // Set var in modal
      $("#modal-title").text(info.event.extendedProps.titleLong);
      $("#modal-candidate").text(info.event.extendedProps.candidate);
      $("#modal-date").text(info.event.extendedProps.formatedStart);
      $("#modal-location").text(info.event.extendedProps.location);
      $("#modal-points").text(info.event.extendedProps.points);
      $("#modal-decision").text(info.event.extendedProps.decision);
      $("#modal-remark").text(info.event.extendedProps.remark);

      $("#modal-edit-button").attr('href', info.event.extendedProps.editUrl);
      $('#modal-edit-button').show();

      $('#modal-teachers ul').empty();
      info.event.extendedProps.teachers.forEach(function(teacher) {
       $('#modal-teachers ul').append("<li>" + teacher + "</li>");
      });

      // Show modal
      $('#modal-interview').modal('show');
    }
  }
});
