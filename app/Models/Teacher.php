<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.05.2019
/// Description: Model for teachers

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends Authenticatable
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idTeacher', 'teaUsername', 'teaPassword', 'teaAcronym', 'teaFirstname', 'teaLastname', 'teaEmail','teaIsAdmin', 'teaGCAccessToken'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_teacher';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'idTeacher';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Indicates if the timestamps are saved.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The deleted at timestamp for the model.
     *
     * @var string
     */
    const DELETED_AT = 'teaDeletedAt';

    /**
      * Get the password for the user.
      *
      * @return string
      */
    public function getAuthPassword() {
        return $this->teaPassword;
    }

    /**
     * The interviews that the teacher has
     *
     * By default ordered by interview's start descending
     *
     * @return array of Interview
     */
    public function interviews() {
        return $this->belongsToMany(Interview::class, 't_interview_teacher', 'fkTeacher', 'fkInterview')->orderBy('intStart','desc');
    }

    /**
     * Overrides the method to ignore the remember token.
     */
    public function setAttribute($key, $value) {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute) {
          parent::setAttribute($key, $value);
        }
    }

    /**
     * Get formated firstname and lastname ('firstname lastname')
     * @return string full name
     */
    public function fullname(){
      return $this->teaFirstname.' '.$this->teaLastname;
    }

    /**
     * Get all teacher expect admin
     * @return array of Teacher
     */
    public static function expectAdmin(){
        return Teacher::all()->reject(function ($teacher) {
            return $teacher->teaUsername == 'admin';
        });
    }
}
