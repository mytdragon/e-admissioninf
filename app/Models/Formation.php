<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.05.2019
/// Description: Model for formations

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Formation extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'idFormation', 'forCode', 'forName'
    ];

    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 't_formation';

    /**
    * The primary key for the model.
    *
    * @var string
    */
    protected $primaryKey = 'idFormation';

    /**
    * Indicates if the IDs are auto-incrementing.
    *
    * @var bool
    */
    public $incrementing = true;

    /**
    * Indicates if the timestamps are saved.
    *
    * @var bool
    */
    public $timestamps = false;

    /**
    * The Candidate that choose the formation.
    *
    * @return array of Candidate
    */
    public function candidate() {
        return $this->hasMany(Candidate::class, 'fkFormation');
    }
}
