<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.05.2019
/// Description: Model for decisions

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Decision extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idDecision', 'decCode', 'decName'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_decision';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'idDecision';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Indicates if the timestamps are saved.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The interviews that have the decision
     *
     * @return array of Interview
     */
    public function interviews() {
        return $this->hasMany(Interview::class, 'fkInterview');
    }
}
