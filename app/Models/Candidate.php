<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.05.2019
/// Description: Model for teachers

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Candidate extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idCandidate', 'canNo', 'canFirstname', 'canLastname'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_candidate';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'idCandidate';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Indicates if the timestamps are saved.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The deleted at timestamp for the model.
     *
     * @var string
     */
    const DELETED_AT = 'canDeletedAt';

    /**
     * Override parent boot and Call deleting event
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function($candidate) {
           foreach ($candidate->interviews()->get() as $interview) {
              $interview->delete();
           }
        });
    }

    /**
     * The interviews that the candidate has
     *
     * By default ordered by interview's start descending
     *
     * @return array of Interview
     */
    public function interviews() {
        return $this->hasMany(Interview::class, 'fkCandidate')->orderBy('intStart','desc');
    }

    /**
    * The formation that choose the candidate.
    *
    * @return Formation
    */
    public function formation() {
        return $this->belongsTo(Formation::class, 'fkFormation', 'idFormation');
    }

    /**
     * Get formated firstname and lastname ('firstname lastname')
     * @return string full name
     */
    public function fullname(){
        return $this->canFirstname.' '.$this->canLastname;
    }
}
