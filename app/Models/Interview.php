<?php
/// ETML
/// Author: Loïc Herzig
/// Date: 06.05.2019
/// Description: Model for interviews

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interview extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'idInterview', 'intStart', 'intEnd', 'intLocation', 'intRemark', 'intPoints'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_interview';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'idInterview';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'intStart', 'intEnd'
    ];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Indicates if the timestamps are saved.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The deleted at timestamp for the model.
     *
     * @var string
     */
    const DELETED_AT = 'intDeletedAt';

    /**
    * The candidate whose admission interview is intended.
    *
    * @return Candidate
    */
    public function candidate() {
      return $this->belongsTo(Candidate::class, 'fkCandidate', 'idCandidate');
    }

    /**
    * The decision taken in the interview.
    *
    * @return Decision
    */
    public function decision() {
        return $this->belongsTo(Decision::class, 'fkDecision', 'idDecision');
    }

    /**
     * The teachers for the interview
     *
     * By default ordered by teacher's lastname ascending
     *
     * @return array of Teacher
     */
    public function teachers() {
          return $this->belongsToMany(Teacher::class, 't_interview_teacher', 'fkInterview', 'fkTeacher')->orderBy('teaLastname','asc');
    }

    /**
     * Get the teacher by acronym and in one string
     * Format: XXX & ... & XXX
     * @return string Formated string
     */
    public function teachersAcronymsString(){
        return $this->teachers->pluck('teaAcronym')->implode(' & ');
    }
}
