<?php
/**
  * ETML
  * Author: Loïc Herzig
  * Date: 24.05.2019
  */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * iCalendar exportation's Mailable class
 */
class ExportICal extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The ics file string.
     *
     * @var string
     */
    protected $ics;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ics)
    {
        $this->ics = $ics;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.ical')
                ->subject('e-admissionINF - Exportation iCal')
                ->from(config('mail.from.address'), config('mail.from.name'))
                ->attachData($this->ics, 'cal.ics', [
                    'mime' => 'text/calendar',
                ]);
    }
}
