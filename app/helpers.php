<?php
/**
  * ETML
  * Author: Loïc Herzig
  * Date: 16.05.2019
  * Descriptions: List of functions that can be called from anywhere
  */

/**
 * Set capital on first char of string
 * @param  string $string   String to edit
 * @return string           String with capital on first char
 */
function mb_ucfirst($string)
{
    $fc = mb_strtoupper(mb_substr($string, 0, 1));
    return $fc.mb_substr($string, 1);
}
