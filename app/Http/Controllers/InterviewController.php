<?php
/**
  * ETML
  * Author: Loïc Herzig
  * Date: 08.05.2019
  */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Models\Interview;
use App\Models\Decision;
use App\Models\Candidate;
use App\Models\Teacher;

use Mail;
use App\Mail\ExportICal;

use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;

use PhpOffice\PhpSpreadsheet\Shared\Date as DateExcel;

/**
 * Interview's controller class
 */
class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('interview.index', [
          'interviews' => Interview::all(),
          'decisions' => Decision::all(),
          'candidates' => Candidate::all(),
          'teachers' => Teacher::expectAdmin()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('interview.form', [
          'action' => 'add',
          'decisions' => Decision::all(),
          'candidates' => Candidate::all(),
          'teachers' => Teacher::expectAdmin()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'start.date' => 'required|date|date_format:Y-m-d|after_or_equal:'.now()->format('d.m.Y'),
          'start.hour' => 'required|numeric|between:0,23',
          'start.minute' => 'required|numeric|between:0,59',
          'location' => 'nullable|string|max:50',
          'remark' => 'nullable|string',
          'points' => 'nullable|integer',
          'decision' => 'nullable|numeric',
          'candidate' => 'required|numeric',
          'teachers' => 'required|array|min:2',
          'teachers.*' => 'numeric'
        ]);

        $start = $request->start['date'].' '.sprintf("%02d", $request->start['hour']).':'.sprintf("%02d", $request->start['minute']);
        $startDateTime = \DateTime::createFromFormat('Y-m-d H:i', $start);

        $endDateTime = \DateTime::createFromFormat( 'Y-m-d H:i', $startDateTime->format('Y-m-d H:i') );
        $endDateTime->add(new \DateInterval('PT'.config('app.interview_length').'M'));

        // Check existence of teacher before save and map
        $teachers = collect($request->teachers)->map(function ($teacher){
            return Teacher::findOrFail($teacher);
        })->reject(function ($teacher) {
            return $teacher->teaUsername == 'admin';
        });

        // create
        $interview = new Interview();
        $interview->intStart = $startDateTime;
        $interview->intEnd = $endDateTime;
        $interview->intLocation = $request->location;
        $interview->intRemark = $request->remark;
        $interview->intPoints = $request->points;
        if ($request->decision){
            $interview->decision()->associate(Decision::findOrFail($request->decision));
        }
        $interview->candidate()->associate(Candidate::findOrFail($request->candidate));
        $interview->save();

        // Add teachers relation
        foreach ($teachers as $teacher){
            $interview->teachers()->attach($teacher);
        }

        $request->session()->flash('message', ['result' => true, 'title' => __('gest.addSucces.title'), 'msg' => __('gest.addSucces.msg', ['model' => $interview->candidate->fullname()])]);
        return redirect()->route('interviews.index');
    }

    /**
     * Display the specified resource in json if ajax or redirect in index view.
     *
     * @param  \App\Models\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Interview $interview)
    {
        // Return json formated interview for ajax
        if ($request->ajax()){
          return response()->json([
            'id' => $interview->idInterview,
            'start' => $interview->intStart->format('d.m.Y H:i'),
            'end' => $interview->intEnd->format('d.m.Y H:i'),
            'location' => $interview->intLocation,
            'remark' => $interview->intRemark,
            'points' => $interview->intPoints,
            'decision' => $interview->decision ? $interview->decision->decName : '',
            'candidate' => $interview->candidate->fullname(),
            'teachers' => $interview->teachers->map(function ($teacher) {return $teacher->fullname();})
          ]);
        } else {
          return redirect()->route('interviews.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function edit(Interview $interview)
    {
        $data = [
          'action' => 'edit',
          'interview' => $interview,
          'decisions' => Decision::all()
        ];

        if (Auth::user()->teaIsAdmin){
          $data['candidates'] = Candidate::all();
          $data['teachers'] = Teacher::expectAdmin();
        }

        return view('interview.form', $data);
    }

    /**
     * Go to the update function according to user permission.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Interview $interview)
    {
        if (Auth::user()->teaIsAdmin) { return Self::updateAdmin($request, $interview); }
        else { return Self::updateTeacher($request, $interview); }
    }

    /**
     * Update the specified resource in storage for admin.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    private function updateAdmin(Request $request, Interview $interview)
    {
        $rules = [
          'start.date' => 'required|date|date_format:Y-m-d',
          'start.hour' => 'required|numeric|between:0,23',
          'start.minute' => 'required|numeric|between:0,59',
          'location' => 'nullable|string|max:50',
          'remark' => 'nullable|string',
          'points' => 'nullable|integer',
          'decision' => 'nullable|numeric',
          'candidate' => 'required|numeric',
          'teachers' => 'required|array|min:2',
          'teachers.*' => 'numeric'
        ];

        // Check if end is after start
        $start = $request->start['date'].' '.$request->start['hour'].':'.$request->start['minute'];
        $startDateTime = \DateTime::createFromFormat('Y-m-d H:i', $start);

        $endDateTime = \DateTime::createFromFormat( 'Y-m-d H:i', $startDateTime->format('Y-m-d H:i') );
        $endDateTime->add(new \DateInterval('PT'.config('app.interview_length').'M'));

        // if date are different, it means we will change so I need to add validation on the moment
        if ($interview->intEnd->format('Y-m-d') != $request->start['date']){
          $rules['start.date'] = $rules['start.date'].'|after_or_equal:now';
        }

        $validatedData = $request->validate($rules);

        // Check existence of teacher before save and map
        $teachers = collect($request->teachers)->map(function ($teacher){
          return Teacher::findOrFail($teacher);
        })->reject(function ($teacher) {
          return $teacher->teaUsername == 'admin';
        });

        // update
        $interview->intStart = $startDateTime;
        $interview->intEnd = $endDateTime;
        $interview->intLocation = $request->location;
        $interview->intRemark = $request->remark;
        $interview->intPoints = $request->points;
        if ($request->decision){
          $interview->decision()->associate(Decision::findOrFail($request->decision));
        }
        $interview->candidate()->associate(Candidate::findOrFail($request->candidate));
        $interview->save();
        $interview->teachers()->sync($teachers->pluck('idTeacher'));

        $request->session()->flash('message', ['result' => true, 'title' => __('gest.editSucces.title'), 'msg' => __('gest.editSucces.msg', ['model' => $interview->candidate->fullname()])]);
        return redirect()->route('interviews.index');
    }

    /**
     * Update the specified resource in storage for teacher.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    private function updateTeacher(Request $request, Interview $interview)
    {
        $rules = [
          'remark' => 'nullable|string',
          'points' => 'nullable|integer',
          'decision' => 'nullable|numeric'
        ];

        $validatedData = $request->validate($rules);

        // update
        $interview->intRemark = $request->remark;
        $interview->intPoints = $request->points;
        if ($request->decision){
          $interview->decision()->associate(Decision::findOrFail($request->decision));
        }
        $interview->save();

        $request->session()->flash('message', ['result' => true, 'title' => __('gest.editSucces.title'), 'msg' => __('gest.editSucces.msg', ['model' => $interview->candidate->fullname()])]);
        return redirect()->route('agenda.show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function destroy(Interview $interview)
    {
        $interview->delete();
    }

    /**
     * Import interviews by reading excel file (xls, xlsx, xlsm)
     * @param  Request $request
     * @return Redirect
     */
    public function import(Request $request)
    {
        $validatedData = $request->validate([
            'file' => 'required|file|mimes:xls,xlsx,xlsm'
        ]);

        // Get file from request
        $file = $request->file('file');

        // Load file and sheet with datas
        $spreadsheet = IOFactory::load($file);
        $worksheetName = config('import.interviews.sheet_name');
        $worksheet = $spreadsheet->getSheetByName($worksheetName);

        if (!$worksheet){
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.sheetNotFound.title'), 'msg' => __('gest.sheetNotFound.msg', ['name' => $worksheetName])]);
            return redirect()->route('candidates.index');
        }

        $rowIterator = $worksheet->getRowIterator(config('import.interviews.header_row'));
        $cellIteratorHeader = $rowIterator->current()->getCellIterator();

        $noHeader = config('import.interviews.header_no');
        $firstnameHeader = config('import.interviews.header_firstname');
        $lastnameHeader = config('import.interviews.header_lastname');
        $dateHeader = config('import.interviews.header_date');
        $timeHeader = config('import.interviews.header_time');
        $teachersHeader = config('import.interviews.header_teachers');
        foreach ($cellIteratorHeader as $cell) {
            switch ($cell->getFormattedValue()) {
                case $noHeader:
                    $noColumn = $cell->getColumn();
                    break;

                case $firstnameHeader:
                    $firstnameColumn = $cell->getColumn();
                    break;

                case $lastnameHeader:
                    $lastnameColumn = $cell->getColumn();
                    break;

                case $dateHeader:
                    $dateColumn = $cell->getColumn();
                    break;

                case $timeHeader:
                    $timeColumn = $cell->getColumn();
                    break;

                case $teachersHeader:
                    $teachersColumn = $cell->getColumn();
                    break;

                default:
                    // Nothing to do...
                    break;
            }
        }

        if (!isset($noColumn)){
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.headerNoNotFound.title'), 'msg' => __('gest.headerNoNotFound.msg', ['header' => $noHeader])]);
            return redirect()->route('interviews.index');
        }
        if (!isset($firstnameColumn)){
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.headerFirstnameNotFound.title'), 'msg' => __('gest.headerFirstnameNotFound.msg', ['header' => $firstnameHeader])]);
            return redirect()->route('interviews.index');
        }
        if (!isset($lastnameColumn)){
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.headerLastnameNotFound.title'), 'msg' => __('gest.headerLastnameNotFound.msg', ['header' => $lastnameHeader])]);
            return redirect()->route('interviews.index');
        }
        if (!isset($dateColumn)){
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.headerDateNotFound.title'), 'msg' => __('gest.headerDateNotFound.msg', ['header' => $dateHeader])]);
            return redirect()->route('interviews.index');
        }
        if (!isset($timeColumn)){
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.headerTimeNotFound.title'), 'msg' => __('gest.headerTimeNotFound.msg', ['header' => $timeHeader])]);
            return redirect()->route('interviews.index');
        }
        if (!isset($teachersColumn)){
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.headerTeachersNotFound.title'), 'msg' => __('gest.headerTeachersNotFound.msg', ['header' => $teachersHeader])]);
            return redirect()->route('interviews.index');
        }

        // Loop in rows and extract candidate data we want
        $interviewRow = config('import.interviews.header_row') + 1;
        if ($interviewRow > $worksheet->getHighestRow()){
            $request->session()->flash('message', ['result' => true, 'title' => __('gest.importInterviewsSucces.title'), 'msg' => __('gest.importInterviewsSucces.msg')]);
            return redirect()->route('interviews.index');
        }
        $rowIterator = $worksheet->getRowIterator(config('import.interviews.header_row') + 1);
        $interviews = [];
        foreach ($rowIterator as $row){
            $no = $worksheet->getCell($noColumn.$row->getRowIndex())->getFormattedValue();
            // Exit if empty row
            if ($no == ''){ break; }

            $firstname = mb_ucfirst(mb_strtolower($worksheet->getCell($firstnameColumn.$row->getRowIndex())->getFormattedValue()));
            if ($firstname == ''){
                $request->session()->flash('message', ['result' => false, 'title' => __('gest.importCandidateFirstnameMissing.title'), 'msg' => __('gest.importCandidateFirstnameMissing.msg', ['no' => $no])]);
                return redirect()->route('interviews.index');
            }
            $lastname = mb_ucfirst(mb_strtolower($worksheet->getCell($lastnameColumn.$row->getRowIndex())->getFormattedValue()));
            if ($lastname == ''){
                $request->session()->flash('message', ['result' => false, 'title' => __('gest.importCandidateLastnameMissing.title'), 'msg' => __('gest.importCandidateLastnameMissing.msg', ['no' => $no])]);
                return redirect()->route('interviews.index');
            }
            $candidate = Candidate::where([
                ['canNo', $no],
                ['canFirstname', $firstname],
                ['canLastname', $lastname]
            ])->first();

            if (!$candidate){
                $request->session()->flash('message', ['result' => false, 'title' => __('gest.candidateNotExistWithCodeAndFullname.title'), 'msg' => __('gest.candidateNotExistWithCodeAndFullname.msg', ['no' => $no,'firstname' => $firstname,'lastname' => $lastname])]);
                return redirect()->route('interviews.index');
            }

            $excelDate = $worksheet->getCell($dateColumn.$row->getRowIndex())->getValue();
            if ($excelDate == ''){
                $request->session()->flash('message', ['result' => false, 'title' => __('gest.importInterviewDateMissing.title'), 'msg' => __('gest.importInterviewDateMissing.msg', ['no' => $no])]);
                return redirect()->route('interviews.index');
            }

            $time = $worksheet->getCell($timeColumn.$row->getRowIndex())->getFormattedValue();
            if ($time == ''){
                $request->session()->flash('message', ['result' => false, 'title' => __('gest.importInterviewTimeMissing.title'), 'msg' => __('gest.importInterviewTimeMissing.msg', ['no' => $no])]);
                return redirect()->route('interviews.index');
            }

            if (!is_numeric($excelDate) || !str_contains($time, 'h')){
                $request->session()->flash('message', ['result' => false, 'title' => __('gest.importInterviewDateFormatInvalid.title'), 'msg' => __('gest.importInterviewDateFormatInvalid.msg', ['no' => $no])]);
                return redirect()->route('interviews.index');
            }

            $date = DateExcel::excelToDateTimeObject($excelDate);
            $start = \DateTime::createFromFormat('d.m.Y G\hi', $date->format('d.m.Y').' '.$time);
            if ($start === false){
                $request->session()->flash('message', ['result' => false, 'title' => __('gest.importInterviewDateFormatInvalid.title'), 'msg' => __('gest.importInterviewDateFormatInvalid.msg', ['no' => $no])]);
                return redirect()->route('interviews.index');
            }

            $teachers = $worksheet->getCell($teachersColumn.$row->getRowIndex())->getFormattedValue();
            if ($teachers == ''){
                $request->session()->flash('message', ['result' => false, 'title' => __('gest.importInterviewTeachersMissing.title'), 'msg' => __('gest.importInterviewTeachersMissing.msg', ['no' => $no])]);
                return redirect()->route('interviews.index');
            }
            $teachers = collect(explode(' & ', $teachers))
            ->reject(function ($acronym) {
                return $acronym == '';
            })
            ->mapWithKeys(function ($acronym) {
                return [$acronym => Teacher::where('teaAcronym', $acronym)->first()];
            });

            if ($teachers->contains(null)){
                $request->session()->flash('message', ['result' => false, 'title' => __('gest.teacherNotExistWithAcronym.title'), 'msg' => __('gest.teacherNotExistWithAcronym.msg',['acronym' => $teachers->search(null)])]);
                return redirect()->route('interviews.index');
            }

            $end = \DateTime::createFromFormat( 'n/j/Y G\hi', $start->format('n/j/Y G\hi') );
            $end->add(new \DateInterval('PT'.config('app.interview_length').'M'));
            $interviews[] = [
                'start' => $start,
                'end' => $end,
                'candidate' => $candidate,
                'teachers' => $teachers
            ];
        }

        // Validate datas
        $validator = Validator::make($interviews, [
            '*' => 'array',
            '*.start'    => 'required|date',
            '*.end'  => 'required|date',
            '*.candidate'  => 'required',
            '*.teachers' => 'required'
        ]);

        if ($validator->fails()) {
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.importDataInvalid.title'), 'msg' => __('gest.importDataInvalid.msg')]);
            return redirect()->route('interviews.index');
        }

        foreach ($interviews as $interview){
            $_interview = new Interview();
            $_interview->intStart = $interview['start'];
            $_interview->intEnd = $interview['end'];
            $_interview->candidate()->associate($interview['candidate']);
            $_interview->save();

            foreach ($interview['teachers'] as $teacher){
                $_interview->teachers()->save($teacher);
            }
        }

        $request->session()->flash('message', ['result' => true, 'title' => __('gest.importInterviewsSucces.title'), 'msg' => __('gest.importInterviewsSucces.msg')]);
        return redirect()->route('interviews.index');
    }

    /**
     * Generate iCalendar file with interviews
     * @param  Request $request
     * @return ics file
     */
    public function exportICal(Request $request, $mail = false)
    {
        if (!$request->ajax()) { return redirect()->route('agenda.show'); }

        if ($request->has('mail')) { $mail = $request->mail; }

        if ($mail){
            // Check if user have set email
            $email = Auth::user()->teaEmail;
            if (!$email) {
                return response()->json([
                    'error' => 'Vous n\'avez pas réglé votre adresse mail.'
                ]);
            }
        }

        $interviews = collect($request->interviews)->map(function($id) {
            return Interview::findOrFail($id);
        });

        $vCalendar = new Calendar(config('app.name'));

        foreach ($interviews as $interview){
            $vEvent = new Event();
            $vEvent
              ->setDtStart($interview->intStart)
              ->setDtEnd($interview->intEnd)
              ->setSummary('Entretien d\'admission de '.$interview->candidate->fullname())
              ->setLocation($interview->intLocation);
            $vCalendar->addComponent($vEvent);
        }

        header('Content-Type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename="cal.ics"');

        $ics = $vCalendar->render();

        if ($mail){
            Mail::to($email)
                ->send(new ExportICal($ics));
        }

        return $ics;
    }
}
