<?php
/**
  * ETML
  * Author: Loïc Herzig
  * Date: 07.02.2019
  */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;
use App\Models\Teacher;
use Session;
use Lang;

/**
 * Custom Auth Class.
 */
class AuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Show the usual login form
     * @return \Illuminate\View\View|\Illuminate\Http\RedirectResponse Return login form or redirect back
     */
    public function showLoginForm() {
      if (Auth::Check())
        return redirect()->back();

      return view('auth.login');
    }

    /**
     * Custom auth a user
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse redirect to post form
     */
    public function login(Request $request) {
      // Validate inputs
      $this->validate($request, [
        'username' => 'required|max:20',
        'password' => 'required',
      ]);

      // block the login if too many attempts
      if ($this->hasTooManyLoginAttempts($request)) {
          $this->fireLockoutEvent($request);

          // Return the block message
          return $this->sendLockoutResponse($request);
      }

      // Test the credentials
      if (Auth::attempt(['teaUsername' => $request->username,'password' => $request->password])) {
        $this->clearLoginAttempts($request);

        // Redirect on the main page
        return redirect()->route('home');
      }

      // Increment number attemps
      $this->incrementLoginAttempts($request);

      return redirect()->back()
        ->withInput($request->only("username"))
        ->withErrors([
            "password" => Lang::get('auth.fail'),
         ]);
    }

    /**
     * Logout the user
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request) {
      Auth::Logout();
      return redirect('login');
    }

    /**
     * Method override Redirect the user after determining they are locked out.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendLockoutResponse(Request $request) {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        return redirect()->back()
            ->withInput($request->only("username"))
            ->withErrors([
                'password' => [Lang::get('auth.throttle', ['seconds' => $seconds])],
            ]);
    }

    /**
     * Update the authenticated user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'username' => 'nullable|string|max:20',
            'password' => 'nullable|string|max:50',
            'email' => 'nullable|email|max:255',
        ]);

        if ($request->username){
          // Check if username is not taken
          if (Teacher::where('teaUsername', $request->username)->exists() && $request->username != Auth::user()->teaUsername){
            $request->session()->flash('message', ['result' => false, 'title' => Lang::get('gest.usernameAlreadyTaken.title'), 'msg' => Lang::get('gest.usernameAlreadyTaken.msg')]);
            return redirect()->back()->withInput();
          }

          Auth::user()->teaUsername = $request->username;
        }

        if ($request->password){
          Auth::user()->teaPassword = Hash::make($request->password);
        }

        Auth::user()->teaEmail = $request->email;
        Auth::user()->save();

        $request->session()->flash('message', ['result' => true, 'title' => __('gest.selfEditSuccess.title'), 'msg' => __('gest.selfEditSuccess.msg')]);
        return redirect()->route('auth.show');
    }
}
