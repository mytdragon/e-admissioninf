<?php
/**
  * ETML
  * Author: Loïc Herzig
  * Date: 09.05.2019
  */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

/**
 * Agenda's stuffs controller class
 */
class AgendaController extends Controller
{
    /**
     * Display the agenda
     * @return View
     */
    public function show()
    {
        $interviews = Auth::user()->interviews->map(function($interview){
            return [
                'id' => $interview->idInterview,
                'title' => $interview->candidate->fullname(),
                'titleLong' => 'Entretien '.$interview->candidate->fullname(),
                'start' => $interview->intStart->format('Y-m-d H:i'),
                'end' => $interview->intEnd->format('Y-m-d H:i'),
                'formatedStart' => $interview->intStart->format('d.m.Y H:i'),
                'formatedEnd' => $interview->intEnd->format('d.m.Y H:i'),
                'candidate' => $interview->candidate->fullname(),
                'location' => $interview->intLocation,
                'remark' => $interview->intRemark,
                'points' => $interview->intPoints,
                'decision' => $interview->decision ? $interview->decision->decName : '',
                'candidate' => $interview->candidate->fullname(),
                'teachers' => $interview->teachers->map(function ($teacher) {return $teacher->fullname();}),
                'editUrl' => route('interviews.edit', ['interview' => $interview->idInterview])
            ];
        })->toJson();

        return view('agenda', ['interviews' => $interviews]);
    }
}
