<?php
/**
  * ETML
  * Author: Loïc Herzig
  * Date: 09.05.2019
  */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Models\Candidate;
use App\Models\Formation;

/**
 * Candidate's controller class
 */
class CandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('candidate.index', ['candidates' => Candidate::orderBy('canNo')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidate.form', [
            'action' => 'add',
            'formations' => Formation::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'no' => 'required|integer',
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'formation' => 'required|numeric',
        ]);

        // Check if formation exist
        $formation = Formation::findOrFail($request->formation);

        // Create
        $candidate = new Candidate();
        $candidate->canNo = $request->no;
        $candidate->canFirstname = $request->firstname;
        $candidate->canLastname = $request->lastname;
        $candidate->formation()->associate($formation);
        $candidate->save();

        $request->session()->flash('message', ['result' => true, 'title' => __('gest.addSucces.title'), 'msg' => __('gest.addSucces.msg', ['model' => $candidate->fullname()])]);
        return redirect()->route('candidates.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function edit(Candidate $candidate)
    {
        return view('candidate.form', [
            'action' => 'edit',
            'candidate' => $candidate,
            'formations' => Formation::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Candidate $candidate)
    {
        $validatedData = $request->validate([
            'no' => 'required|integer',
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'formation' => 'required|numeric',
        ]);

        // Check if formation exist
        $formation = Formation::findOrFail($request->formation);

        // Create
        $candidate->canNo = $request->no;
        $candidate->canFirstname = $request->firstname;
        $candidate->canLastname = $request->lastname;
        $candidate->formation()->associate($formation);
        $candidate->save();

        $request->session()->flash('message', ['result' => true, 'title' => __('gest.editSucces.title'), 'msg' => __('gest.editSucces.msg', ['model' => $candidate->fullname()])]);
        return redirect()->route('candidates.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Candidate $candidate)
    {
        $candidate->delete();
    }

    /**
     * Import candidates by reading excel file (xls, xlsx, xlsm)
     * @param  Request $request
     * @return Redirect
     */
    public function import(Request $request)
    {
        $validatedData = $request->validate([
            'file' => 'required|file|mimes:xls,xlsx,xlsm'
        ]);

        // Get file from request
        $file = $request->file('file');

        // Load file and sheet with datas
        $spreadsheet = IOFactory::load($file);
        $worksheetName = config('import.candidates.sheet_name');
        $worksheet = $spreadsheet->getSheetByName($worksheetName);

        if (!$worksheet){
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.sheetNotFound.title'), 'msg' => __('gest.sheetNotFound.msg', ['name' => $worksheetName])]);
            return redirect()->route('candidates.index');
        }

        $rowIterator = $worksheet->getRowIterator(config('import.candidates.header_row'));
        $cellIteratorHeader = $rowIterator->current()->getCellIterator();

        $noHeader = config('import.candidates.header_no');
        $firstnameHeader = config('import.candidates.header_firstname');
        $lastnameHeader = config('import.candidates.header_lastname');
        $formationHeader = config('import.candidates.header_formation');
        foreach ($cellIteratorHeader as $cell) {
            switch ($cell->getFormattedValue()) {
                case $noHeader:
                    $noColumn = $cell->getColumn();
                    break;

                case $firstnameHeader:
                    $firstnameColumn = $cell->getColumn();
                    break;

                case $lastnameHeader:
                    $lastnameColumn = $cell->getColumn();
                    break;

                case $formationHeader:
                    $formationColumn = $cell->getColumn();
                    break;

                default:
                    // Nothing to do...
                    break;
            }
        }

        if (!isset($noColumn)){
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.headerNoNotFound.title'), 'msg' => __('gest.headerNoNotFound.msg', ['header' => $noHeader])]);
            return redirect()->route('candidates.index');
        }
        if (!isset($firstnameColumn)){
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.headerFirstnameNotFound.title'), 'msg' => __('gest.headerFirstnameNotFound.msg', ['header' => $firstnameHeader])]);
            return redirect()->route('candidates.index');
        }
        if (!isset($lastnameColumn)){
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.headerLastnameNotFound.title'), 'msg' => __('gest.headerLastnameNotFound.msg', ['header' => $lastnameHeader])]);
            return redirect()->route('candidates.index');
        }
        if (!isset($formationColumn)){
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.headerFormationNotFound.title'), 'msg' => __('gest.headerFormationNotFound.msg', ['header' => $formationHeader])]);
            return redirect()->route('candidates.index');
        }

        // Loop in rows and extract candidate data we want
        $candidateRow = config('import.candidates.header_row') + 1;
        if ($candidateRow > $worksheet->getHighestRow()){
            $request->session()->flash('message', ['result' => true, 'title' => __('gest.importCandidatesSucces.title'), 'msg' => __('gest.importCandidatesSucces.msg')]);
            return redirect()->route('candidates.index');
        }
        $rowIterator = $worksheet->getRowIterator($candidateRow);
        $candidates = [];
        foreach ($rowIterator as $row){
            $no = $worksheet->getCell($noColumn.$row->getRowIndex())->getFormattedValue();
            $firstname = mb_ucfirst(mb_strtolower($worksheet->getCell($firstnameColumn.$row->getRowIndex())->getFormattedValue()));
            $lastname = mb_ucfirst(mb_strtolower($worksheet->getCell($lastnameColumn.$row->getRowIndex())->getFormattedValue()));
            $formationCode = $worksheet->getCell($formationColumn.$row->getRowIndex())->getFormattedValue();
            $formation = Formation::where('forCode', $formationCode)->first();

            if (!$formation){
                $request->session()->flash('message', ['result' => false, 'title' => __('gest.formationNotExistWithCode.title'), 'msg' => __('gest.formationNotExistWithCode.msg', ['code' => $formationCode])]);
                return redirect()->route('candidates.index');
            }

            $candidates[] = [
                'no' => $no,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'formation' => $formation
            ];
        }

        // Validate datas
        $validator = Validator::make($candidates, [
            '*' => 'array',
            '*.no'    => 'required|integer',
            '*.firstname'  => 'required|string',
            '*.lastname'  => 'required|string',
            '*.formation' => 'required'
        ]);

        if ($validator->fails()) {
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.importDataInvalid.title'), 'msg' => __('gest.importDataInvalid.msg')]);
            return redirect()->route('candidates.index');
        }

        foreach ($candidates as $candidate){
            $_candidate = new Candidate();
            $_candidate->canNo = $candidate['no'];
            $_candidate->canFirstname = $candidate['firstname'];
            $_candidate->canLastname = $candidate['lastname'];
            $_candidate->formation()->associate($candidate['formation']);
            $_candidate->save();
        }

        $request->session()->flash('message', ['result' => true, 'title' => __('gest.importCandidatesSucces.title'), 'msg' => __('gest.importCandidatesSucces.msg')]);
        return redirect()->route('candidates.index');
    }
}
