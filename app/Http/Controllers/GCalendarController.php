<?php
/**
  * ETML
  * Author: Loïc Herzig
  * Date: 13.05.2019
  */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;
use App\Models\Interview;

/**
 * Google Calendar and API stuff's controller class
 */
class GCalendarController extends Controller
{
    /**
     * The google api client
     */
    protected $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Initialise the client.
        $this->client = new Google_Client();
        // Set the application name, this is included in the User-Agent HTTP header.
        $this->client->setApplicationName(config('app.name'));
        // Set the authentication credentials we downloaded from Google.
        $this->client->setAuthConfig([
          'client_id' => config('google.client_id'),
          'client_secret' => config('google.client_secret')
        ]);
        // Setting offline here means we can pull data from the venue's calendar when they are not actively using the site.
        $this->client->setAccessType("offline");
        // This will include any other scopes (Google APIs) previously granted by the venue
        $this->client->setIncludeGrantedScopes(true);
        // Set this to force to consent form to display.
        $this->client->setApprovalPrompt('force');
        // Add the Google Calendar scope to the request.
        $this->client->addScope(Google_Service_Calendar::CALENDAR);
        // Set the redirect URL back to the site to handle the OAuth2 response. This handles both the success and failure journeys.
        $this->client->setRedirectUri(route('gcalendar.oauth2callback'));
    }

    /**
     * Redirect to the token request google form
     * @return Redirect
     */
    public function show()
    {
        if (Auth::user()->teaGCAccessToken) { return redirect()->route('auth.update'); }
        $this->client->setState(Auth::user()->idTeacher);
        return redirect($this->client->createAuthUrl());
    }

    /**
     * Store the token got.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Check if the state match authenticated user's id
        if ($request->state != Auth::user()->idTeacher){
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.associationEchec.title'), 'msg' => __('gest.associationEchec.msg')]);
            return redirect()->route('auth.update');
        }

        if ($request->has('error')){
            if ($request->error == 'access_denied'){
                return redirect()->route('auth.update');
            }
        }

        if ($request->has('code')){
            $token = $this->client->authenticate($request->code);
            Auth::user()->teaGCAccessToken = json_encode($token);
            Auth::user()->save();

            $request->session()->flash('message', ['result' => true, 'title' => __('gest.associationSuccess.title'), 'msg' => __('gest.associationSuccess.msg')]);
            return redirect()->route('auth.update');
        }

        $request->session()->flash('message', ['result' => false, 'title' => __('gest.associationEchec.title'), 'msg' => __('gest.associationEchec.msg')]);
        return redirect()->route('auth.update');
    }

    /**
     * Refresh User token if needed
     * @return null
     */
    public function refreshToken()
    {
        $this->client->setAccessToken(Auth::user()->teaGCAccessToken);
        if ($this->client->isAccessTokenExpired()) {
          $token = $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
          Auth::user()->teaGCAccessToken = json_encode($token);
          Auth::user()->save();
        }
    }

    /**
     * Revoke the token of the authenticated user
     * @param  Request $request
     * @return Redirect
     */
    public function revokeToken(Request $request)
    {
        if (!Auth::user()->teaGCAccessToken) { return redirect()->route('auth.update'); }
        Self::refreshToken();
        $token = json_decode(Auth::user()->teaGCAccessToken);
        $revoke = $this->client->revokeToken($token->access_token);

        if ($revoke){
          Auth::user()->teaGCAccessToken = null;
          Auth::user()->save();
          $request->session()->flash('message', ['result' => true, 'title' => __('gest.dissociationSuccess.title'), 'msg' => __('gest.dissociationSuccess.msg')]);
        }
        else {
            $request->session()->flash('message', ['result' => false, 'title' => __('gest.dissociationEchec.title'), 'msg' => __('gest.dissociationEchec.msg')]);
        }

        return redirect()->route('auth.update');
    }

    /**
     * Export interviews on google calendar account
     * @param  Request $request
     * @return Response|null Json error or null if success
     */
    public function export(Request $request)
    {
        if (!$request->ajax()) { return redirect()->route('agenda.show'); }

        // Check if user associated google calendar
        if (!Auth::user()->teaGCAccessToken) {
            return response()->json([
                'error' => 'Vous n\'avez pas lié de compte Google Calendar.'
            ]);
        }

        $interviews = collect($request->interviews)->map(function($id) {
            return Interview::findOrFail($id);
        });

        Self::refreshToken();
        $service = new Google_Service_Calendar($this->client);
        foreach ($interviews as $interview){
            // Creat event and insert
            $googleStartTime = new Google_Service_Calendar_EventDateTime();
            $googleStartTime->setTimeZone(config('app.timezone'));
            $googleStartTime->setDateTime($interview->intStart);

            $googleEndTime = new Google_Service_Calendar_EventDateTime();
            $googleEndTime->setTimeZone(config('app.timezone'));
            $googleEndTime->setDateTime($interview->intEnd);

            $event = new Google_Service_Calendar_Event();
            $event->setStart($googleStartTime);
            $event->setEnd($googleEndTime);
            $event->setSummary('Entretien d\'admission de '.$interview->candidate->fullname());
            $event->setLocation($interview->intLocation);
            $service->events->insert('primary', $event);
        }
    }
}
